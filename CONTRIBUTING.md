# Contributing to the Website's source code

The Website of [fedi.foundation][website] uses [Jekyll] to generate static HTML files from Markdown files.\
We always welcome contributions that help to improve the website, be it performance improvements, a11y enhancements or even small tweaks to the site's theme.

> If you only want to update an existing post or create a new one, visit [this page][publish] for instructions on how to do this.

## Prerequisites

In order to contribute to the source code will you need to have made the following steps before starting:

- You created a [Codeberg Account][codeberg].
- You installed [Jekyll][jekyll-installation] on your PC.
- You installed [NPM][npm-installation] on your PC.
- You installed [Git][git-installation] on your PC.

## Getting started

1. [Fork the Repository][fork]
2. Clone your fork (Not the original repository) to your PC using git bash:\
   ```sh
   git clone https://codeberg.org/YOUR_USERNAME/fedi-foundation.git
   ```
3. `cd` into the cloned repository:\
   ```sh
   cd fedi-foundation
   ```
4. **Recommended:** Set `fediverse/fedi-foundation` as Upstream repository to yours:\
   ```sh
   git remote add upstream https://codeberg.org/fediverse/fedi-foundation.git
   ```
5. **Recommended:** Check that your clone is up-to-date (Requires step 4):\
   - Fetch latest changes from Upstream:\
     ```sh
     git fetch upstream
     ```
   - Checkout main branch:\
     ```sh
     git checkout main:\
     ```
   - Merge any changes from upstream's main into your branch:\
     ```sh
     git merge upstream/main
     ```
6. **Recommended:** Create a new branch for your changes:\
   ```sh
   git checkout -b your-branch-name
   ```
7. You are now ready to edit your stuff.

## Testing

In case you've made changes to the site's behaviour or design is it recommended to first test your changes before committing and submitting your changes for review.

To test your changes, do the following:

1. Follow the [Prerequisites](#prerequisites) if you haven't already.
2. Run `npm install`
3. Run `bundle install`
4. Run `bundle exec jekyll serve`
5. If everything goes well should a live-preview be served at http://127.0.0.1:4000

## Proposing your changes

Depending on your git settings will you be required to first add the added, changed or deleted files to git to commit:\
```sh
git add path/to/file
```
After that commit your changes to your remote repository:\
```sh
git commit -m "A concise and useful commit message should be here."
```
Now you can push your changes. Should you use a newly made branch and the remote doesn't have it yet are you required to add `--set-upstream <branch>` to the command:
```sh
# Use this if your branch already has one on the remote
git push
# Use this if your remote doesn't have a branch linked yet
git push --set-upstream your-branch-name
```
Finally go to the `fediverse/fedi-foundation`'s Pull request tab, open a new Pull request, select the branch of your fork, define a good title and description and press the create button.

> **Tip:** You can prefix your Pull request title with `WIP:` to turn it into a Draft Pull request.

<!-- Website links -->
[website]: https://fedi.foundation
[jekyll]: https://jekyllrb.com
[codeberg]: https://codeberg.org

<!-- installation pages -->
[jekyll-installation]: https://jekyllrb.com/docs/installation/
[npm-installation]: https://docs.npmjs.com/cli/v8/configuring-npm/install
[git-installation]: https://git-scm.com/downloads

<!-- Misc -->
[publish]: https://fedi.foundation/publish.html
[fork]: https://codeberg.org/repo/fork/16961