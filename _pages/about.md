---
title: "About Us"
layout: page-sidebar
permalink: "/about.html"
---

Welcome to the Federated Diversity Foundation! The online magazine where [Social Coding](https://coding.social) meets the [Fediverse](https://fediverse.party).

## We are a Movement

![Social Coding • Foundation • Ecosystem • Fediverse](/assets/images/posts/foundation-lightbulb.png)

Free software development is to large extent a social activity. Successful projects are where many different people with different backgrounds and skills closely interact and collaborate. During the **Free Software Development Lifecycle** or FSDL, many challenges must be overcome. The **Social Coding Movement** explores best-practices as well as tools that are available to help technologists.

The Fediverse is the ideal social networking environment that can facilitate the many processes of the FSDL. We want the Fediverse to support inclusive free software development. In doing so we will evolve a vibrant ecosystem where we can use the Fediverse for its own development. So that the Fediverse can truly shine, and Social Coding projects will flourish. 

- **We empower community.** Strong communities build strong ecosystems. We are dedicated to the success of all Social Coding Practitioners. We help each other to move ahead.

- **We foster cooperation.** Creating a solid technology foundation is a collective effort. We streamline organization and processes to make that easier. Encouraging fedizen interaction, and establishing relationships across the web.

- **We facilitate healthy growth.** We want more technologists to join the fray. To make both Fediverse and FOSS healthy and strong. We evolve our open standards, and help expand the ecosystem of free software projects that interoperate.

- **We advocate our work**. More people should know what we do, and why we do it. We are the Peopleverse, moving on a vision of "Social Networking Reimagined". With Humane Technology we'll be United in Diversity.

## We are Social Coders

Content on this site is created collectively by our entire member base. Are you a Social Coder that is involved with making the Fediverse a better place? Do you have something you want to tell the broader community of fellow fedizens? Whether you want to announce your project or a new release, have ideas for the future of the Fediverse, or want to advocate its unique aspects to the world at large.. do not hesitate. Become a Fedi Foundation author and publish an article to this site. Together we will spread the message far and wide.

Learn [How You Can Publish On Fedi Foundation](/publish.html).

## We help build the Fediverse

[The Fediverse is like Spiral Island](/2021/04/fediverse-spiral-island-analogy/): A tiny human-made structure floating in rough seas. It is lovely, vibrant and holds a promise for a better future. But its continued existence is by no means secure. There are storms to weather. Only with concerted effort, and people cooperating from all across the globe, can it become a sprawling archipelago. A place of beauty and humanity. A true Peopleverse that ties the online world neatly to our daily lives.

The [Social Coding Movement](https://coding.social) and its co-shared [community](https://discuss.coding.social), are among its builders. We invite you to help build with us, and would love to welcome you in our midst.

## Support our work

Fedi foundation is maintained and operated by a team of dedicated Social Coding community members. Community work is _very_ time-consuming. It includes the boring tasks, doing the chores, so that others benefit and have a great experience. It is essential, even vital work, to maintain community health.

Your help is needed so we can pay for infrastructure, and provide small compensation for those who volunteer their time to do this noble work. Read [How You Can Contribute](/donate.html) and for what we use your support.