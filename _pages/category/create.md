---
layout: category
category:
  name: create
  tags: [research, specification, fep, code]
  image: "building.svg"
related:
  - title: "ActivityPub Application Watchlist"
    url: "https://git.feneas.org/feneas/fediverse/-/wikis/watchlist-for-activitypub-apps"
  - title: "ActivityPub Developer Resources Watchlist"
    url: "https://git.feneas.org/feneas/fediverse/-/wikis/watchlist-for-activitypub-developer-resources"
  - title: "FEP: Fediverse Enhancement Proposals"
    url: "https://git.activitypub.dev/ActivityPubDev/Fediverse-Enhancement-Proposals"
  - title: "Spritely: Social Worlds Await"
    url: "https://spritelyproject.org/"
  - title: "DREAM: Peer-to-Peer Group Collaboration"
    url: "https://dream.public.cat/"
  - title: "Unofficial ActivityPub Testsuite"
    url: "https://test.activitypub.dev/"
permalink: "/create.html"
---
Open standards and specifications, collaborative research, design patterns and quality code. All required ingredients to build a Fediverse that thrives. They are vital for a healthy evolution of our decentralized ecosystem. Here we'll focus on design, architecture, implementation and project management to streamline creation of great federated apps.