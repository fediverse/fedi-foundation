---
title: "Community"
layout: community
category: community
permalink: "/community"
---
Coding is social. With a set of [Principles](https://coding.social/principles.html) as our guid we dedicate our time to the benefit of FOSS projects, their ecosystems, and to all fedizens on the Fediverse. Cooperation for us is very important. In fact, it is vital. Without close social interaction andy open standards or healthy technology landscape, and interoperability between our apps would be impossible. We strive to take Fediverse to the next level, far into the future. As Social Coding Practitioners [**_"Reimagining Social Networking"_**](/2022/09/social-networking-reimagined/) is part of what we do.

We invite you to join us in our quest, to discover our work, and help create humane technology. It does not matter what skills you have, whether you want to develop or help spread our message. We need everyone, so hesitate no longer. Get involved!