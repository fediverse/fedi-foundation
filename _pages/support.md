---
title: "Support the Social Coding Movement"
layout: page-sidebar
permalink: "/support.html"
---

[Social Coding Movement](https://coding.social) is the grassroots community of technologists and fedizens that dedicate themselves to the success  Free Software and the Fediverse. Together we are [**"United in Diversity"**](/about/) and volunteer time and effort on a common quest to envision [**"Social Networking Reimagined"**](/2022/09/social-networking-reimagined/) and the social processes of the Free Software Development Lifecycle (FSDL).

The Fediverse is still but fragile, its future by no means secure. What we build is analogous to [**"Spiral Island"**](/2021/04/fediverse-spiral-island-analogy/), a human-made structure floating in rough seas. We as Social Coders help extend its base: The Fediverse technology foundation, upon which anyone can build great social experiences.

We want to shape beautiful archipelago's that delight people with what they provide. And we really need your help to enable us to do so. Here's what you can do right now:

- Sign up to [Discuss Social Coding](https://discuss.coding.social) and get to know fellow members.
- Don't be shy to participate in discussion and start new topics.
- You don't need to be tech-savvy. We welcome any passionate person.
- Good starting places are [Fediverse Futures](https://discuss.coding.social/c/foundations/fediversefutures/24) and [Community](https://discuss.coding.social/c/community/2) categories.
- Also give feedback in the [Ideas](https://discuss.coding.social/c/ideation-hub/ideas/20) section, or add your own idea.

Sooo... in other words: [We'd Love to Welcome You &nbsp;<i class="fa fa-heart" aria-hidden="true"></i>&nbsp; in Our Midst.](https://discuss.coding.social/t/welcome-to-discuss-social-coding-forum/7)

---

Currently we do not receive any financial support for our contributions. But given the importance of community work, this may change in the future. Join us directly on chat in [Social Coding Chatroom](https://matrix.to/#/#socialcoding-movement:matrix.org) on Matrix. Meet fellow social coders and share opinion and feedback.

Thank you so much!
