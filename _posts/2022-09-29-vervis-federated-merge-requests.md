---
title: "Vervis Demo 4: Federated Merge Requests"
description: "Welcome to the 4th ForgeFed demo! In this demo, you can create a Git repository on a Vervis instance, and then open a merge request from a fork you create on another Vervis instance."
author: fr33domlover
categories: ideate
tags: [design, demo, cooperation]
status: featured
image: /assets/images/posts/pexels-james-wheeler-1578750.jpg
---

Welcome to the 4th [ForgeFed](https://forgefed.org) demo! These instructions demonstrate opening and merging a Federated Merge Request.

## (0) Intro

Going through these instructions may require some basic familiarity with Git repositories and the basic Merge Request workflow.

<dl>
  <dt>Forge</dt>
  <dd>A website where people host their (mostly software) projects and collaborate on them together</dd>

  <dt>Forge software</dt>
  <dd>Software used to run a forge website; for example, the <a href="https://forgejo.org/" target="_blank" rel="noopener">Forgejo</a> software powers the <a href="https://codeberg.org" target="_blank" rel="noopener">Codeberg</a> website, the <a href="https://gitlab.com/gitlab-org/gitlab" target="_blank" rel="noopener">GitLab CE</a> software powers the <a href="https://framagit.org" target="_blank" rel="noopener">Framagit</a> website, the <a href="https://sourcehut.org" target="_blank" rel="noopener">Sourcehut</a> software powers the <a href="https://sr.ht" target="_blank" rel="noopener">sr.ht</a> website</dd>

  <dt>Federation</dt>
  <dd>The ability of people using different websites to interact with each other as if they were using the same website, because the websites have a common communication language/protocol using which they talk to each other, thus forming a decentralized network of websites and people</dd>

  <dt>Instance</dt>
  <dd>A website powered by some specific web server software; e.g. <a href="https://codeberg.org" target="_blank" rel="noopener">Codeberg</a> is a <a href="https://forgejo.org/" target="_blank" rel="noopener">Forgejo</a> instance, <a href="https://floss.social" target="_blank" rel="noopener">floss.social</a> is a <a href="https://joinmastodon.org" target="_blank" rel="noopener">Mastodon</a> instance</dd>

  <dt>Forge Federation</dt>
  <dd>A network of federated connected forges, where people on different forges can freely collaborate on projects, and people can host projects on any forge of their choice, including self-hosting their own forge</dd>

  <dt>ForgeFed</dt>
  <dd>A common language for forges to communicate and federate, an extension of the <a href="https://activitypub.rocks" target="_blank" rel="noopener">ActivityPub</a> federation protocol</dd>

  <dt>Vervis</dt>
  <dd>Pre-alpha under-construction experimental forge software, used for demonstration, development and experimentation of forge federation using ForgeFed, and as a reference implementation for other forge software to test against</dd>
</dl>

### What's in this demo

In this demo, you can create a Git repository on a Vervis instance, and then open a merge request from a fork you create on another Vervis instance.

### Previous demos

I mentioned it's the "4th demo", so you may wonder where the previous 3 demos are! The information in them is outdated, but for the very curious, here they are:

- Demo 1, Federated ticket comments, 2019
- Demo 2, Federated opening of new tickets, July 2019, [archived on the wayback machine](https://web.archive.org/web/20220201003645/https://talk.feneas.org/t/vervis-demo-2-federated-opening-of-new-tickets/78)
- Demo 3, Push activities and repo following, November 2019, [archived on the wayback machine](https://web.archive.org/web/20220201002301/https://talk.feneas.org/t/vervis-demo-3-push-activities-and-repo-following/177)

## (1) Setup

On your computer, generate an SSH key to use for pushing Git commits:

```shell
ssh-keygen -t rsa -f ./vervis-demo-key
```

<p class="info">
Tech note: Only RSA keys work at the moment, it won't work with Ed25519 keys or anything else
</p>

Add the following to your `~/.ssh/config`:

```
Host fig.fr33domlover.site
    Port 5022
    ControlMaster no
    ForwardAgent no
    ForwardX11 no
    IdentityFile ~/vervis-demo-key
    KexAlgorithms +diffie-hellman-group1-sha1
    Ciphers aes128-ctr,aes192-ctr,aes256-ctr,aes128-cbc,3des-cbc

Host grape.fr33domlover.site
    Port 6022
    ControlMaster no
    ForwardAgent no
    ForwardX11 no
    IdentityFile ~/vervis-demo-key
    KexAlgorithms +diffie-hellman-group1-sha1
    Ciphers aes128-ctr,aes192-ctr,aes256-ctr,aes128-cbc,3des-cbc

Host walnut.fr33domlover.site
    Port 7022
    ControlMaster no
    ForwardAgent no
    ForwardX11 no
    IdentityFile ~/vervis-demo-key
    KexAlgorithms +diffie-hellman-group1-sha1
    Ciphers aes128-ctr,aes192-ctr,aes256-ctr,aes128-cbc,3des-cbc
```

<p class="info">
Those SSH config sections are needed because the Vervis instances don't use the standard SSH port 22, but also because the SSH server component is a bit outdated :sweat_smile:
</p>

## (2) Create a project

Create an account on the [fig.fr33domlover.site][fig] Vervis instance.

Once you've verified your email address and logged in, the homepage will have an *SSH key settings* link. Upload your **public** key, i.e. the `vervis-demo-key.pub` file.

Back on the homepage, there's a *Create a new repository* link. Create a **Git** repository.

<p class="info" markdown="1">
[Darcs](http://darcs.net) repos are supported by Vervis as well, but at the moment this demo fully works only with Git repos
</p>

Back on the homepage, there's a *Create a new patch tracker* link. You'll be asked to pick a repo. Pick the repo you just created.

Back on the homepage, there's a list of your repos. Go to the repo you created. You'll see a message about the new Patch Tracker. Click the ***Link*** button.

On your computer, create a Git repo and add some content:

```shell
git init --initial-branch=main vervis-demo-repo
cd vervis-demo-repo
echo "Hello world!" > file.md
git add file.md
git commit --message="Write some awesome text"
```

Push the commit(s). Grab the **SSH** push URL from the repo's page on Vervis. Reminder, the homepage has a list of your repos.

```shell
git push --set-upstream LOGIN@fig.fr33domlover.site:REPOCODE main
```

You can keep the terminal open, we'll use it again in a bit.

## (3) Create a fork

Create an account on the [grape.fr33domlover.site][grape] Vervis instance.

Once you've verified your email address and logged in, the homepage will have an *SSH key settings* link. Upload your **public** key, i.e. the `vervis-demo-key.pub` file.

Back on the homepage, there's a *Create a new repository* link. Create a **Git** repository.

On your computer, make some more commits in the local repo you created:

```shell
echo "Have you tried the cool ForgeFed demo yet?" >> file.md
git add file.md
git commit --message="Add more cool content"
```

Push the new commit(s) to the **new** repo you just created on the **grape** instance. The repo's page on Vervis has the **SSH** push URL.

```shell
git remote add fork LOGIN@grape.fr33domlover.site:OTHERCODE
git push fork main
```

## (4) Open a merge request

Create an account on the [walnut.fr33domlover.site][walnut] Vervis instance.

Once you've verified your email address and logged in, the homepage should have an *Open a merge request* link. Specify the **fig** patch tracker URL, its repo's page URL as the target, and the **grape** repo's URL as the origin. If you followed the above instructions, both origin and target branches are *main*. Reminder, the homepages have lists your repos and patch trackers.

```
Tracker         https://fig.fr33domlover.site/looms/...
Target Repo     https://fig.fr33domlover.site/repos/...
Target branch   main
Origin Repo     https://grape.fr33domlover.site/repos/...
Origin branch   main
```

<p class="info">
On Vervis, the terms "Merge Request", "Pull Request" and "Patch" mean almost the same thing, and are often interchangeable.
</p>

After submitting the form, on the link bar at the top of the page there's an "outbox" link. There you'll see a list of scary looking JSON objects :sweat_smile: make sure the first one is from a few seconds ago.

Now go back to the [fig][] instance. The homepage should have a link to your Patch Tracker, and there you'll see the new Merge Request you opened. Under "Bundle ... patches" there's list the commits in the MR, available to view as Git patches.

## (5) Merge the merge request

On the MR's page (under the Patch Tracker on the [fig][] instance), there's an *Apply* button. Hopefully the the status says "Can apply" :blush: Good luck!

Once the MR is successfuly applied, go to the repo's page, click *Commits* and notice that the fork's commits have been merged into the upstream repo.

End of demo!

Have fun :blush:

-- The ForgeFed team

## (A) Authors

Written by: [fr33domlover](https://todon.nl/@fr33domlover)

Edited by: Loïc Dachary <loic@dachary.org> and [Andre601](https://blobfox.coffee/@andre_601)

## (B) Some notes

* We did some testing, but there may still be bugs and mistakes! If anything is confusing or fails or you have questions or comments, come talk with us! :smiley:
    * Comment [on the Fediverse](https://floss.social/@forgefed/109080573073843411)
    * IRC channel: `#forgefed` on Libera Chat
    * [Matrix room](https://matrix.to/#/#forgefed:libera.chat)

[fig]: https://fig.fr33domlover.site
[grape]: https://grape.fr33domlover.site
[walnut]: https://walnut.fr33domlover.site

<br>

<small>_(Photo: [Pathway Surrounded By Fir Trees](https://www.pexels.com/photo/photo-of-pathway-surrounded-by-fir-trees-1578750/) by [James Wheeler](https://www.pexels.com/@souvenirpixels/))_</small>

<!--
  Image source: https://www.pexels.com/photo/photo-of-pathway-surrounded-by-fir-trees-1578750/
  License: https://www.pexels.com/license/
  License Description: Free use, no attribution requirements
  Photo Credit: James Wheeler, https://www.pexels.com/@souvenirpixels/
-->