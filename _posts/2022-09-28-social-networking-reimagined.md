---
title: "Let's Reimagine Social"
description: "People are social creatures, and social networking is part of human nature. Social Media have clouded our vision and distorted what 'social' means. The Social Coding Movement aims to Reimagine Social Networking and envision a Peopleverse."
author: aschrijver
categories: ideate
tags: [vision, design, brainstorm]
status: sticky
image: /assets/images/posts/pexels-cottonbro-4881619.jpg
---

Megacorporations like Google, Facebook and Twitter and their influential platforms erode our social cohesion and have become a menace to our society. We must reaffirm what Social truly means, and reinstate it accordingly.

<p class="summary" markdown="1">
**Summary**

In Fall 2022 the [Social Coding Movement](https://coding.social) sets out on a mission that involves inclusive software development for the Fediverse. This new initiative has set an aim to unlock its full potential. Instead of a focus on separate federating apps the movement will emphasize the _social experiences_ people have. So that Fediverse gives rise to a true Peopleverse, where people can _reimagine social_.
</p>

## Contents

- [What is Social?](#wat-is-social)
- [The Fediverse is Social, right?](#the-fediverse-is-social-right)
- [Social Experience Design](#social-experience-design)
- [The Peopleverse](#the-peopleverse)

## What is Social?

While searching on a [stock photography website](https://www.pexels.com/search/social/) for images depicting "Social", about 80 percent of results were of people mindlessly staring into their smartphone. The so-called _'Phone Zombies'_ we have all become familiar with. And also many depictions of Social Media logo's and related terminology. A Google Image Search yields similar results. There is a total alienation of what we know deep down that "being social" ought to be. Which a dictionary summarizes concisely as:

<p class="info" markdown="1">
[**social**](https://www.wordnik.com/words/social) _sō′shəl_<br>

&nbsp;&nbsp;&nbsp; _1. Of or relating to human society and its modes of organization._<br>
&nbsp;&nbsp;&nbsp; _2. Of or relating to rank and status in society._<br>
&nbsp;&nbsp;&nbsp; _3. Of, relating to, or occupied with matters affecting human welfare._<br>
</p>

Something similar happens when people hear "social network". They immediately associate with online technology. But social networking is what we naturally do in our everyday life, and for many thousands of years. **We are deeply social creatures** that have complex and intricate relationships to each other and the world around us. All of our daily activity has intricate social aspects.

And come the rise of the internet we also do that increasingly online. The so-called "Social" Media giants like Facebook and Twitter and similar platforms have successfully clouded our vision not only with regards to the concept of what social is, but made us see social networks as discrete services they kindly provide us with their walled garden platform. When it comes to the internet it is high time that we reaffirm that..

> **The entire Web is social in nature.**

A related realization is that most of web content has close ties to our social activities that exist offline.

While Social Media involves complex sociology, it has just little to do with what common people would understand to be "social" with one another. Social Media are just like interactive broadcasting channels, that are optimized for _engaging people_, whatever the emotion that engagement involves. And it becomes increasingly obvious that they have huge detrimental effects on society. I am facilitator of Humane Tech Community and in 2019 had an exchange with Tristan Harris, ex-Google ethicist and co-founder of [The Center for Humane Technology](https://humanetech.com) (known from "The Social Dilemma" on Netflix) who told me in all seriousness that social media "will lead to a complete unraveling of the social fabric of society and wars".

## The Fediverse is Social, right?

There is hope, as luckily we have the Fediverse. A proper alternative, a social experiment, that is on the rise. Fediverse, totally grassroots and without monetary incentives and ads, seen in that way is a true _humane technology playground_.

The [W3C ActivityPub Recommendation](https://www.w3.org/TR/activitypub/) upon which the Fediverse is based, describes itself as a _decentralized social networking protocol_. And it has proven very successful over time. A host of [delightful apps](https://delightful.club/delightful-fediverse-apps/) are available, and millions of people use them frequently. The Fediverse is growing and open standards-based social networks may become real alternatives to the dominance of corporate social media.

Yet not all is well. There are [major challenges](https://discuss.coding.social/t/major-challenges-for-the-fediverse/67) to overcome first. With hardly any people dedicated to maintain and evolve open standards and the technology base there is [Protocol Decay](https://www.ietf.org/archive/id/draft-iab-protocol-maintenance-08.html#name-protocol-decay) and increasing complexity. This is detrimental for broader technology adoption and emergence of a richer ecosystem. Introducing new innovations becomes ever harder. 

> **Fediverse is at risk of not being able to reach its full potential.**

Federated application development is a deeply technical exercise. To establish and maintain compatibility and integrations with other apps can be a frustrating task. Tackling the technical complexity requires so much attention and effort from dedicated Free Software developers, that it leaves less opportunity to build vibrant and inclusive communities around their projects. Let alone is there time to spend beyond the app to strengthen and evolve the "technology substrate" upon which the whole Fediverse relies. There's a gap between app developers and regular fedizens that must be bridged.

Above all we must not allow our imagination to be restricted by what current Social Media provide. We can go far beyond what current platforms define "social" to be. If we unleash our creativity the Fediverse can be much more than a mirror image of what the Big Tech giants have to offer people.

## Social experience design

Creating and expanding the web to allow people to interact and find insightful information is a social activity. Because if you come to think of it: **Coding is social** rather than technical. In software development you can only build the right things if you deeply understand the community who you are building for, and that means communicating with everyone involved. The technology is a secondary concern. Seen in this light..

> **All internet-connected apps where people interact are social apps.**

Here then lies the key to recognizing the full potential of the Fediverse. Social is _everywhere!_ So **Let's reimagine social** online.

Come this Fall the [Social Coding Movement](https://coding.social) launches, and has set this to be its Mission. Starting with coding itself she will explore the social aspects of the entire **Free Software Development Lifecycle**, the FSDL. The Fediverse plays a central role in this initiative. It provides the enabling technology for an intricate "social fabric" to be woven.

For that to happen we must look beyond individual apps, as apps are silo's. We are used to them, but they are not natural to the flow of human activity that is more task-oriented. With the **interoperability** of the Fediverse open standards we have opportunity to create deep integrations between online services, and introduce a new paradigm of **app-free computing**.

{: .info}
"App-free computing" is a term coined by Chris Gebhardt of [InfoCentral](https://infocentral.org) in a Lightning Talk presentation given in 2018, called [Unity and Interoperability Among Decentralized Systems](https://infocentral.org/presentations/DWS2018LightningTalk.pdf) (PDF).

To avoid application-centered thinking and overly technical focus, instead of federated app development Social Coding when it comes to the Fediverse will focus on **Social Experience Design**, or SX. The concept was introduced by Toshihiko Yamakami in a 2012 [IEEE paper](https://www.semanticscholar.org/paper/From-User-Experience-to-Social-Experience%3A-A-New-Yamakami/e859d580900368ff5542961948716e336b26e558) and followed by a range of other papers by the same author. The field of SX never really took off to gain an established place next to User Experience Design [to which it positioned itself](https://www.semanticscholar.org/paper/Exploratory-analysis-of-differences-between-social-Yamakami/a9f7bb1928597ab98c1eb299092dbf3ad7c9a686). This may be because social experiences are much harder to define than UX design. They are simply less concrete and can be uniquely personal. In yet another [paper](https://link.springer.com/chapter/10.1007/978-3-642-41674-3_6) a preliminary definition of SX can be like..

{: .info}
"Social Experience Design" deals with the design of the way a person feels about other humans through a computer-user interface.

This vagueness is a Good Thing™ as on the Fediverse we all know that social experiences shouldn't be designed by knowledgeable experts. Instead they emerge from the numerous interactions and collaboration of many people. But using the terminology helps focus on the importance of that as a precondition to having a good time on the Fediverse. It also fits the inclusion and diversity that Social Coding is all about.

> Federated social experiences are deeply integrated apps that are tailored to people's needs and have positive social impact.

We will simply make the concept of SX our own, and give it meaning by the delightful experiences we create. SX will be our way to collectively **reimagine social**.

## The Peopleverse

There is another thing to address for the Fediverse to reach its full potential. There is a general lack of grand dreams of what the Fediverse could be in the future. Both in terms of technical capabilities as well as what exciting things are enabled by those. If we want to be the humane alternative and a decidedly better choice than corporate Social Media, we need to have a common outlook of what is possible. Define a shared vision that will unleash our creativity.

The Fediverse is still this rather technical concept. Federated servers, instances that are de-facto communities, decentralization, self-hosting and a bunch of microblogging related terminology such as following, boosting, blocking, timelines and more. This is fine, but it is application-centric and doesn't do justice to human relationships as we know them intuitively. All the apps tied together by interoperable protocols are just supportive technology that allow humans to extend their social networks online. 

At Social Coding Movement we will adopt such shared vision, and we call it the **Peopleverse**.

Mind you, this is NOT about renaming the Fediverse, but more about _what the Fediverse enables_. Instead of thinking of running apps on it, the Peopleverse deals with **weaving the social fabric** that ties our offline and online social networks seamlessly together, and in support of our daily lives.

> The Peopleverse is the social fabric spun up by the Fediverse.

You might say that in this vision the Fediverse enables the Peopleverse. To phrase it in the Universe analogy: If Fediverse is this vast empty space full of vibrant energy, the Peopleverse is its matter. The constellation of galaxies, stars and planets. Loosely shaped relationships between people and communities, yet all interconnected and interacting through the force of gravity.

_(Note that Peopleverse has no association whatsoever with The Metaverse, which is just some 'cold, heartless business concept'. Nothing more than a product vision that is slung out in front of us, for it to be mindlessly consumed and monetized)._

<br>

<small>_(Photo: [People Gathered in a Circle](https://www.pexels.com/photo/woman-in-gray-hoodie-beside-woman-in-gray-hoodie-4881619/) by [cottonbro](https://www.pexels.com/@cottonbro/))_</small>

<!--
  Image source: https://www.pexels.com/photo/woman-in-gray-hoodie-beside-woman-in-gray-hoodie-4881619/
  License: https://www.pexels.com/license/
  License Description: Free use, no attribution requirements
  Photo Credit: cottonbro, https://www.pexels.com/@cottonbro/
-->
