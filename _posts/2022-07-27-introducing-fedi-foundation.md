---
title: "Introducing the Federated Diversity Foundation"
description: "Where Social Coders encourage a healthy evolution of Fediverse technology."
author: aschrijver
categories: community
tags: [vision, roadmap]
status: featured
image: /assets/images/posts/pexels-anna-shvets-4312861.jpg
---

Welcome to the brand new website of the Federated Diversity Foundation, or simply _fedi foundation_. This article provides more background on why this magazine site was created as a companion to the [Social Coding Movement](https://coding.social), and for what purpose it exists.

#### Contents

- [History of the Social Coding Movement](#history-of-the-social-coding-movement)
- [Barriers to Fediverse evolution](#barriers-to-fediverse-evolution)
- [Fedi Foundation, Social Coding and the Fediverse](#fedi-foundation-social-coding-and-the-fediverse)
  - [Community empowerment](#community-empowerment)
  - [Facilitating cooperation](#facilitating-cooperation)
  - [Stimulating growth](#stimulating-growth)
  - [Boosting advocacy](#boosting-advocacy)
- [Support us](#support-us)

---

## History of the Social Coding Movement

The origin of the movement was a discussion in December 2021 that took place between myself, [@humanetech](https://mastodon.social/@humanetech) and [@CSDUMMI](https://norden.social/@csddumi) in Lemmy's [Fediverse Futures](https://lemmy.ml/c/fediversefutures) community to the post [United Software Development: A new paradigm?](https://lemmy.ml/post/61479). The topic was the vision of federating many of the activities involved with software development. Social networking for technologists would be a paradigm shift indeed. Software development is to large extent a social process, where good communication between everyone involved is instrumental to the success of a project. The Fediverse would be the ideal technology base to support these social activities, and it would enable a more inclusive development process where everyone can be involved. So we decided to act.

Social Coding was born, and later renamed to [Social Coding Movement](https://coding.social). This website, the Federated Diversity Foundation already existed at the time, as a draft created by me. It was positioned as a foundation to the [SocialHub](https://socialhub.activitypub.rocks) community, where Fediverse developers discuss technical issues related to their apps and application of the [ActivityPub](https://www.w3.org/TR/activitypub/) and [ActivityStreams](https://www.w3.org/TR/activitystreams-core/) open standards. It was repositioned in July 2022 to be part of the Social Coding Ecosystem because it fits better in this broader scope.


## Barriers to Fediverse evolution

The Fediverse was created in true grassroots fashion. A delightful social fabric that is home to well over four million fedizens. Together we evolved the technology landscape, plugging holes that still existed in the W3C standards, and building custom extensions on top of them. Many production-ready [federated projects](https://delightful.club/delightful-fediverse-apps/) have proven their worth in real-world applications.

Currently we are at an important stage of our development. The tech world around us is changing rapidly, and not in a good direction. There are many threats for our future, our continued existence. Fediverse is still weak and brittle. At the same time there exist tremendous opportunities to do things better than what the corporate world has in store for us. Fediverse has vast potential. Our vision is one of [**_"Social Networking Reimagined"_**](/2022/09/social-media-reimagined/) and the creation of a true Peopleverse. An online space that is a natural extension to our physical environment and daily lives.

However, there are inhibiting factors that are holding us back. Developers have an overly technical focus, and aboveall prioritize their own project. There is little of time to coordinate more broadly. Our grassroots nature, and even decentralization itself, leads to fragmentation and ad-hoc development. Any form of community organization meets resistance and feels like "herding cats". This slows down our evolution as a collective.

> We lack common focus and direction, clear strategy and processes to attain our vision, and committed people to organize the 'substrate' between projects on which we all rely. **We need substrate formation.**

This is understandable. Software developers just want to code, dedicated to their own projects. Spending time on other things is seen as a distraction. But eventually this means these projects will hurt themselves.

All-in-all the challenges are pretty serious and must be addressed to avoid that Fediverse innovation will stall:

| Challenge | Description |
| :--- | :--- |
| [**Lack of a shared (technology) vision**](https://discuss.coding.social/t/challenge-adopt-a-shared-technology-vision-for-the-fediverse/45) | People just use the Fediverse in its current state without asking what it could be in the future. The potential and opportunities are insufficiently explored. |
| [**Stuck Fediverse Adoption Lifecycle**](https://discuss.coding.social/t/challenge-fixing-the-fediverse-technology-adoption-lifecycle/38) | A combination of factors make onboarding and "crossing the chasm" very hard, and combined with growing complexity in the ecosystem this stifles innovation. |
| [**Hardly any Substrate Formation for the Fediverse**](https://discuss.coding.social/t/challenge-healthy-substrate-formation-for-the-fediverse/63) | The technological landscape is fragmented and opaque, community is dispersed and not very collaborative, while the processes for evolving the ecosystem are immature or undefined. |
| [**No proper representation of the Fediverse**](https://discuss.coding.social/t/challenge-representation-of-the-fediverse/65) | With nobody able to represent the Fediverse, it is not possible to anticipate to developments elsewhere, reach out and collaborate with other communities, and follow collective strategies for Fediverse evolution. |
| [**Complacency and intertia**](https://discuss.coding.social/t/challenge-overcome-complacency-and-inertia-on-the-fediverse/79) | Lack of awareness of the threats we face, and general inability to organize proper ways to mitigate them. |

## Fedi Foundation, Social Coding and the Fediverse

More is needed to go next-level, well beyond Microblogging. We must evolve new versions of our technology standards, and design the capabilities to build code that interoperates at a deeper level. Open issues in the community must be addressed and we need more members to help with that. We must coordinate more, and collaborate with different groups.

This requires a lot of community effort. We need people dedicated to help Fediverse move forwards, who volunteer with organization, community engagement and growth, and doing many of the chores that need to be done. The importance of this time-consuming work is much underappreciated.

> Health and future of any federated project depends directly on the community health and strength or our collaboration. Active participation yields a win-win for all fedizen.

Fedi Foundation supports Social Coding community members that are actively engaged with Fediverse development. To any Social Coding practitioner she serves as inspiring information source about Fediverse evolution and innovation. Objectives are also to stimulate activity related to:

- Community empowerment
- Facilitating cooperation
- Stimulating growth
- Technological advocacy

Read more in-depth information about our work in [About Fedi Foundation](/about/).

### Community empowerment

Empowerment follows from knowing the exact state of the technology landscape and our plans for the future. Furthermore any Social Coder be able to have their voice heard and actively help steer the evolution of the technology base.

### Facilitating cooperation

Streamlining the community organization and processes is an important task. Who does what, and who should you contact? We track open issues in the community, and bring them to attention so they can be dealt with. We help organize events, conferences and hackathons, and provide the community with online tools to do her work.

### Stimulating growth

The Fediverse developer community is but very small. Majority of its members are programmers. Other IT skills are underrepresented. So are minority groups. For the community to be healthy more people with diverse backgrounds and different needs should participate. Fedi Foundation will seek to increase incentives to join the Social Coding Movement.

### Boosting advocacy

The world at large deserves to know about the Fediverse. There are a huge amount of people very unhappy with the traditional social media we have today. And following from that technologists and policy makers should know about better ways of doing things. Of building groundbreaking applications, exploring new capabilities, and doing things that are morally and ethically sound. At Fedi Foundation we'll advocate our exciting story to the world, and we entice others to campaign with us. But as Darius Kazemi advocated at the ActivityPub 2020 Conference, in this quest: ["We Must Play and Win our own Game"](https://conf.tube/w/oPQjje8j6qBFj7NSQ9xCKj)

## Support us

Help Social Coding Movement thrive, so we can support FOSS on the Fediverse, and cocreate our fedi future together. Please [**Support Us**](/support.html) and consider to [**Sign Up**](https://discuss.coding.social) as Social Coding community member. 

<!-- Photo by Anna Shvets from Pexels
     https://www.pexels.com/photo/wood-industry-tools-knife-4312861/
 -->