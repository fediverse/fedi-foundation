module.exports = {
  purge: [
    './_includes/**/*.html',
    './_layouts/**/*.html',
    './_posts/*.md',
    './*.html',
  ],
  darkMode: false,
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: {
        DEFAULT: '#eeeeee',
        dark: '#cccccc',
        darker: '#888888'
      },
      pink: {
        DEFAULT: '#f5007e',
        dark: '#a30054'
      },
      grey: {
        lighter: '#444444',
        light: '#333333',
        DEFAULT: '#222222',
        dark: '#111111'
      },
      yellow: {
        DEFAULT: '#fdbe49',
        darker: '#654c1d',
        dark: '#fdb021',
        light: '#fed280'
      },
      green: '#00f53d',
      blue: {
        light: '#2fd7da',
        DEFAULT: '#1fabad',
        300: '#93c5fd',
        700: '#1d4ed8',
        900: '#1e3a8a'
      },
    },
    fontFamily: {
      sans: [
        'ui-sans-serif',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ],
      serif: ['ui-serif', 'Georgia', 'Cambria', '"Times New Roman"', 'Times', 'serif'],
      mono: [
        'ui-monospace',
        'SFMono-Regular',
        'Menlo',
        'Monaco',
        'Consolas',
        '"Liberation Mono"',
        '"Courier New"',
        'monospace',
      ],
      body: [
        'Verdana', 
        'Geneva', 
        'sans-serif',
      ],
    },
    fontSize: {
      xs: ['0.75rem', { lineHeight: '1rem' }],
      sm: ['0.875rem', { lineHeight: '1.25rem' }],
      base: ['1rem', { lineHeight: '1.5rem' }],
      lg: ['1.125rem', { lineHeight: '1.75rem' }],
      xl: ['1.25rem', { lineHeight: '1.75rem' }],
      '2xl': ['1.5rem', { lineHeight: '2rem' }],
      xxl: ['1.675rem', {lineHeight: '2rem'}], // added
      '3xl': ['1.875rem', { lineHeight: '2.25rem' }],
      '4xl': ['2.25rem', { lineHeight: '2.5rem' }],
      '5xl': ['3rem', { lineHeight: '1' }],
      '6xl': ['3.75rem', { lineHeight: '1' }],
      '7xl': ['4.5rem', { lineHeight: '1' }],
      '8xl': ['6rem', { lineHeight: '1' }],
      '9xl': ['8rem', { lineHeight: '1' }],
    },
    extend: {
      typography: {
        DEFAULT: {
          css: {
            color: '#eeeeee',
            'a, a > strong': {
              color: '#fdbe49',
              textDecoration: 'none',
              '&:hover': {
                textDecoration: 'underline',
              },
            },
            strong: {
              color: '#eeeeee',
            },
            hr: {
              borderColor: '#333333',
            },
            h1: {
              color: '#eeeeee',
            },
            h2: {
              color: '#eeeeee',
            },
            h3: {
              color: '#eeeeee',
              fontWeight: '500',
            },
            h4: {
              color: '#eeeeee',
            },
            blockquote: {
              fontWeight: '700',
              color: '#eeeeee',
            },
            '> ul > li > *:last-child': {
              // Avoid extra space in nested lists.
              // See: https://github.com/tailwindlabs/tailwindcss/discussions/5120
              marginBottom: 0,
            },
            'ol > li::before': {
              color: '#eeeeee'
            },
            thead: {
              borderBottomColor: '#333333',
            },
            'thead th:first-child': {
              paddingLeft: '0.60em',
            },
            'thead th:last-child': {
              paddingRight: '0.60em',
            },
            'thead th': {
              color: '#eeeeee',
              backgroundColor: '#333333',
              verticalAlign: 'middle',
              paddingTop: '0.66em',
            },
            'tbody tr': {
              borderBottomWidth: '1px',
              borderBottomColor: '#333333',
              backgroundColor: '#444444',
            },
            'tbody td:first-child': {
              paddingLeft: '0.60em',
            },
            'tbody td:last-child': {
              paddingRight: '0.60em',
            },
          },
        },
        lg: {
          css: {
            '> ul > li > *:last-child': {
              marginBottom: 0,
            },
            'thead th:first-child': {
              paddingLeft: '0.80em',
            },
            'thead th:last-child': {
              paddingRight: '0.80em',
            },
            'thead th': {
              paddingTop: '0.75em',
            },
            'tbody td:first-child': {
              paddingLeft: '0.75em',
            },
            'tbody td:last-child': {
              paddingRight: '0.75em',
            },
          },
        },
        xl: {
          css: {
            '> ul > li > *:last-child': {
              marginBottom: 0,
            },
            'thead th:first-child': {
              paddingLeft: '0.80em',
            },
            'thead th:last-child': {
              paddingRight: '0.80em',
            },
            'thead th': {
              paddingTop: '0.80em',
            },
            'tbody td:first-child': {
              paddingLeft: '0.80em',
            },
            'tbody td:last-child': {
              paddingRight: '0.80em',
            },
          },
        },
      },
    },
  },
  variants: {
    extend: {
      display: ['last'],
    }
  },
  plugins: [
    require('@tailwindcss/typography')({
      modifiers: ['lg', 'xl'],
    }),
  ],
}